import React from "react";
import { Route, Redirect } from "react-router-dom";

function ProtectedRoute({ isAuth: isAuth }) {
  return (
    <Route
      render={(props) => {
        if (isAuth) {
          return <Chef />;
        } else {
          return (
            <Redirect
              to={{ pathname: "/login", state: { from: props.location } }}
            />
          );
        }
      }}
    />
  );
}

export default ProtectedRoute;
