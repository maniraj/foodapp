import "./App.css";
import Login from "./components/auth/Login";
import Signup from "./components/auth/Signup";
import Chef from "./components/Chef/Chef";
import User from "./components/User/User";
import { BrowserRouter as Router, Route } from "react-router-dom";

function App() {
  return (
    <Router>
      <div className="App">
        <Route path="/login" exact component={Login} />
        <Route path="/signup" component={Signup} />
        <Route path="/chef" component={Chef} />
        <Route path="/user" component={User} />
      </div>
    </Router>
  );
}

export default App;
