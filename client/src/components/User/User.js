import React from "react";
import { withRouter } from "react-router-dom";

function User() {
  return <h1>User Page</h1>;
}

export default withRouter(User);
