import React, { useState, useEffect } from "react";
import Axios from "axios";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import { Button } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
      width: "25ch",
    },
  },
}));

function CreateMenu() {
  const classes = useStyles();
  const [iteamName, setIteamName] = useState("");
  const [iteamNameList, setIteamNameList] = useState([]);

  useEffect(() => {
    Axios.get("http://localhost:3001/api/get").then((response) => {
      setIteamNameList(response.data);
    });
  }, []);

  const addIteam = () => {
    Axios.post("http://localhost:3001/api/insert", {
      iteamName: iteamName,
    });

    setIteamNameList([...iteamNameList, { iteamName: iteamName }]);
  };

  const deleteIteam = (iteam) => {
    Axios.delete(`http://localhost:3001/api/delete/${iteam}`);
  };

  return (
    <div>
      <form className={classes.root} noValidate autoComplete="off">
        <TextField
          id="outlined-basic"
          label="Add Iteam"
          variant="outlined"
          onChange={(e) => {
            setIteamName(e.target.value);
          }}
        />
        <Button
          onClick={addIteam}
          variant="contained"
          color="primary"
          size="large"
        >
          Add
        </Button>
        {iteamNameList.map((val) => {
          return (
            <div>
              <h3>{val.iteamName}</h3>
              <Button
                onClick={() => {
                  deleteIteam(val.iteamName);
                }}
              >
                Delete
              </Button>
            </div>
          );
        })}
      </form>
    </div>
  );
}

export default CreateMenu;
