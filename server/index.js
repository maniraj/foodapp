const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const mysql = require("mysql");

//MySQL Database connection
const db = mysql.createPool({
  host: "localhost",
  user: "root",
  password: "Li-0n@mysql",
  database: "FoodApp",
});

app.listen(3001, () => {
  console.log("running on port 3001");
});

app.use(express.json());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));

//Create to database
app.post("/api/insert", (req, res) => {
  const iteamName = req.body.iteamName;
  const sqlInsert = "INSERT INTO create_iteam (iteamName) VALUES (?)";
  db.query(sqlInsert, [iteamName], (err, result) => {
    console.log(result);
  });
});

//Read from database
app.get("/api/get", (req, res) => {
  const sqlSelect = "SELECT * FROM create_iteam;";
  db.query(sqlSelect, (err, result) => {
    res.send(result);
  });
});

//Delete from database
app.delete("/api/delete/:iteamName", (req, res) => {
  const name = req.params.iteamName;
  const sqlDelete = "DELETE FROM create_iteam WHERE (iteamName) = ?";
  db.query(sqlDelete, name, (err, result) => {
    if (err) console.log(err);
  });
});

//Users registration to database
app.post("/signup", (req, res) => {
  const username = req.body.username;
  const password = req.body.password;

  db.query(
    "INSERT INTO users (username, password) VALUES (?,?)",
    [username, password],
    (err, result) => {
      console.log(err);
    }
  );
});

app.post("/login", (req, res) => {
  const username = req.body.username;
  const password = req.body.password;

  db.query(
    "SELECT * FROM users WHERE username = ? AND password = ?)",
    [username, password],
    (err, result) => {
      if (err) {
        res.send({ err: err });
      }

      if (result.lenth > 0) {
        res.send(result);
      } else {
        res.send({ text: "Username & Password not found" });
      }
    }
  );
});
